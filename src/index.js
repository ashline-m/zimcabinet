import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'

import App from 'components/App/App'

import 'react-responsive-select/dist/ReactResponsiveSelect.css'

import 'styles/index.css'
import 'styles/helper.css'

// import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Router>
    <App />
  </Router>, document.getElementById('root'))
// registerServiceWorker();
