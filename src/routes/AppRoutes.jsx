import React from 'react'

import { Route, Switch, Redirect } from 'react-router-dom'
import { default as Home } from 'pages/Home/HomeContainer'
import { default as Cabinets } from 'pages/Cabinet/Cabinets'

const AppRoutes = props =>
  <div>
    <Switch>
      <Route exact path='/' component={Home} />
      <Route exact path='/cabinets' component={Cabinets} />

      <Route render={() => <Redirect to='/' />} />
    </Switch>
  </div>

export default AppRoutes
