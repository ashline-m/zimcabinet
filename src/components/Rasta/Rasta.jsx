import React from 'react'

import './Rasta.css'

export default () =>
  <div className='RastaContainer'>
    <div className='green' />
    <div className='gold' />
    <div className='red' />
    <div className='brown' />
    <div className='red' />
    <div className='gold' />
    <div className='green' />
  </div>
