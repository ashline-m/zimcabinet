import React from 'react'

import './Footer.css'

export default () =>
  <footer className='Footer p-20'>
    <div className='flex-container f-j-between'>
      <div>left</div>
      <div>right</div>
    </div>
  </footer>
