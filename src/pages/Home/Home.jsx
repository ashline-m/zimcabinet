import React from 'react'

import ReactResponsiveSelect from 'react-responsive-select'

import Footer from 'components/Footer/Footer'
import Rasta from 'components/Rasta/Rasta'

import './Home.css'

export default () =>
  <div>
    <header className='App-header'>
      <h1 className='App-title'>Choose a year to start</h1>
    </header>
    <div className='CabinetPickerContainer rel'>
      <Rasta />
      <div className='CabinetPicker'>
        <ReactResponsiveSelect
          caretIcon={<span style={{float: 'right'}}>+</span>}
          name='make'
          options={[
                { text: 'Any', value: 'null' },
                { text: 'Oldsmobile', value: 'oldsmobile', markup: <span>Oldsmobile</span> },
                { text: 'Ford', value: 'ford', markup: <span>Ford</span> }
          ]}
          onChange={this.reportChange}
          onSubmit={() => { this.form.submit() }}
          prefix='Make:'
          selectedValue='mazda'
            />
      </div>
    </div>
    <Footer />
  </div>
